Meteor.methods
	mailNotification : (emails) ->
		if not Meteor.userId()
			throw new Meteor.Error 'invalid-user', "[methods] sendInvitationEmail -> Invalid user"

		rfcMailPattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
		validEmails = _.compact _.map emails, (email) -> return email if rfcMailPattern.test email

		for email in validEmails
			@unblock()

			Email.send
				to: email
				from:  Meteor.user().emails[0].address
				subject: 'Invite'
				html:  '<div><h4>You have been invited to the case <h4><p>Please check Lexhub <br><br><a href ="https://lexhub.scalingo.io" >https://lexhub.scalingo.io/</a></p> </div>'

			console.log "send success to #{email}"

		return 'success'
