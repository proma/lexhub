Meteor.methods
	sendInviteEmail: (options)->
		if not Meteor.userId()
			throw new Meteor.Error 'invalid-user', "[methods] sendSMTPTestEmail -> Invalid user"

		room = RocketChat.models.Rooms.findOneById options.rid

		body = '<div><h4>You have been invited to the case @'+ room.name + ' <h4><p>Please follow link bellow to join conversation<br><br><a href =' + options.url+ ' >' + options.url + '</a></p> </div>'

		@unblock()

		Email.send
			to: options.to
			from: Meteor.user().emails[0].address
			subject : 'Invitation to case'
			html: body

		return {
			message: "Your invite was sent to "
			params: options.to
		}




DDPRateLimiter.addRule
	type: 'method'
	name: 'sendInviteEmail'
	userId: (userId) ->
		return true
, 1, 1000

