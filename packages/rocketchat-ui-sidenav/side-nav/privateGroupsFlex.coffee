Template.casesFlex.helpers
	selectedUsers: ->
		return Template.instance().selectedUsers.get()

	name: ->
		return Template.instance().selectedUserNames[this.valueOf()]

	selectedLawyers: ->
		return Template.instance().selectedLawyers.get()

	lawyer: ->
		return Template.instance().selectedLawyersNames[this.valueOf()]

	groupName: ->
		return Template.instance().groupName.get()

	error: ->
		return Template.instance().error.get()

	autocompleteSettings: ()->
		return {
			limit: 10
			# inputDelay: 300
			rules: [
				{
					# @TODO maybe change this 'collection' and/or template
					collection: 'UserAndRoom'
					subscription: 'roomSearch'
					field: 'username'
					template: Template.userSearch
					noMatchTemplate: Template.userSearchEmpty
					matchAll: true
					filter:
						type: 'u'
						$and: [
							{ _id: { $ne: Meteor.userId() } }
							{ username: { $nin: Template.instance().selectedUsers.get() } }
							{ active: { $eq: true } }
							{ roles: { $ne: 'lawyer' } }
						]
					sort: 'username'
				}
			]
		}

	autocompleteSettingsForLawyers: ()->
		return {
			limit: 10

			rules: [
				{

					collection: 'UserAndRoom'
					subscription: 'roomSearch'
					field: 'username'
					template: Template.userSearch
					noMatchTemplate: Template.userSearchEmpty
					matchAll: true
					filter:
						type: 'u'
						$and: [
							{ _id: { $ne: Meteor.userId() } }
							{ username: { $nin: Template.instance().selectedUsers.get() } }
							{ active: { $eq: true } }
							{ roles: { $ne: 'user' } }
						]
					sort: 'username'
				}
			]
		}


Template.casesFlex.events
	'autocompleteselect #pvt-group-members': (event, instance, doc) ->
		instance.selectedUsers.set instance.selectedUsers.get().concat doc.username

		instance.selectedUserNames[doc.username] = doc.name

		event.currentTarget.value = ''
		event.currentTarget.focus()

	'autocompleteselect #pvt-group-lawyers': (event, instance, doc) ->
		instance.selectedLawyers.set instance.selectedLawyers.get().concat doc.username
		instance.selectedLawyersNames[doc.username] = doc.name

		event.currentTarget.value = ''
		event.currentTarget.focus()

	'click .remove-room-member': (e, instance) ->
		self = @
		users = Template.instance().selectedUsers.get()
		users = _.reject Template.instance().selectedUsers.get(), (_id) ->
			return _id is self.valueOf()

		Template.instance().selectedUsers.set(users)

	'click .remove-room-lawyer': (e, instance) ->
		self = @
		lawyers = Template.instance().selectedLawyers.get()
		lawyers = _.reject Template.instance().selectedLawyers.get(), (_id) ->
			return _id is self.valueOf()

		Template.instance().selectedLawyers.set(lawyers)

	'click .cancel-pvt-group': (e, instance) ->
		SideNav.closeFlex ->
			instance.clearForm()

	'click header': (e, instance) ->
		SideNav.closeFlex ->
			instance.clearForm()

	'mouseenter header': ->
		SideNav.overArrow()

	'mouseleave header': ->
		SideNav.leaveArrow()

	'keydown input[type="text"]': (e, instance) ->
		Template.instance().error.set([])

	'click #case-form-submit': (e, instance) ->
		emailToInvite = instance.find('#pvt-group-members').value
		name = instance.find('#case-name').value.toLowerCase().trim()
		instance.groupName.set name
		members = instance.selectedUsers.get().concat instance.selectedLawyers.get()
		if emailToInvite?
			$('#pvt-group-members').addClass 'validate'
														 .attr 'type' , 'email'

			Session.set 'membersToInvite' , emailToInvite
			Session.set 'caseName' , name
		Meteor.call 'createPrivateGroup', name,members , (err, result) ->
			FlowRouter.go 'group',  name: name

	'keypress  #pvt-group-members': (e,instance) ->
		if instance.find('#pvt-group-members').value is '' and instance.$('#pvt-group-members').hasClass('validate')
			$('#pvt-group-members').removeClass 'validate'
			 											 .removeAttr 'type'

Template.casesFlex.onCreated ->
	instance = this
	instance.selectedUsers = new ReactiveVar []
	instance.selectedLawyers = new ReactiveVar []
	instance.selectedUserNames = {}
	instance.selectedLawyersNames = {}
	instance.error = new ReactiveVar []
	instance.groupName = new ReactiveVar ''

	instance.clearForm = ->
		instance.error.set([])
		instance.groupName.set('')
		instance.selectedUsers.set([])
		instance.selectedLawyers.set([])
		instance.find('#pvt-group-name').value = ''
		instance.find('#pvt-group-members').value = ''
