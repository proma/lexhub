Meteor.methods
	setAdminStatus: (userId, admin) ->
		if not Meteor.userId()
			throw new Meteor.Error 'invalid-user', "[methods] setAdminStatus -> Invalid user"

		unless RocketChat.authz.hasPermission( Meteor.userId(), 'assign-admin-role') is true
			throw new Meteor.Error 'not-authorized', '[methods] setAdminStatus -> Not authorized'

		if admin
			RocketChat.authz.addUserRoles( userId, 'admin')
		else
			RocketChat.authz.removeUserFromRoles( userId, 'admin')

		return true

	setLawyerStatus : (userId, admin) ->
		if not Meteor.userId()
			throw new Meteor.Error 'invalid-user', "[methods] setAdminStatus -> Invalid user"

		unless RocketChat.authz.hasPermission( Meteor.userId(), 'assign-admin-role') is true
			throw new Meteor.Error 'not-authorized', '[methods] setAdminStatus -> Not authorized'

		if admin
			RocketChat.authz.addUserRoles( userId, 'lawyer')
			RocketChat.authz.removeUserFromRoles( userId, 'user')
		else
			RocketChat.authz.removeUserFromRoles( userId, 'lawyer')
			RocketChat.authz.addUserRoles( userId, 'user')

		return true
